# Daltenda discounts 
Controlla la presenza di sconti dal sito Daltenda e li confronta con Amazon.

## Installazione

Requisito [Python](https://www.python.org/), [Selenium](https://www.selenium.dev/) e [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/).

```bash
pip install selenium, beautifulsoup4
```