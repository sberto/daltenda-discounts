from bs4 import BeautifulSoup
from selenium import webdriver
import requests

amz_elemnts_number = 10 # How many Amazon elements to take in account
driver = webdriver.Firefox(executable_path='./geckodriver.exe')

def weight_avg(list):
    if not list:
        return 0

    wSum = sum(w for [el,w] in list)
    if wSum <= 0:
        return 0
    
    return sum(el*w for [el,w] in list)*1.0/wSum

def daltenda(product):
    try:
        name = product.find('a', class_='product-name').text
        name = name.split('CONFE')[0]

        price = product.find('span', class_='price product-price').text
        price = float(price.split('€')[0].replace(',','.'))
        return {'name' : name, 'price' : price}
    except:
        print('Not found.')
        raise

def amazon(query):
    driver.get("https://www.amazon.it/s?k="+query)
    soup2 = BeautifulSoup(driver.page_source, features="html.parser")
    products = soup2.find('div', class_='s-main-slot').contents[:amz_elemnts_number]

    review_list = []
    price_list = []
    for pr in products:
        try:
            people = 0
            review = 0
            review_box = pr.find('div', class_='a-row a-size-small').find_all('span', recursive=False)

            try:
                review = float(review_box[0]["aria-label"].split(' ')[0].replace(',','.'))
                people = int(review_box[1]["aria-label"])
                review_list.append([review,people])
            except:
                pass
            try:
                price = float(pr.find('span','a-price').contents[0].text[:5].replace(',','.'))
                price_list.append([price,people])
            except:
                pass
            
        except:
            continue

    price = weight_avg(price_list)
    review = weight_avg(review_list)

    if price == 0 and review == 0:
        print('No price and review')
        raise

    return {'review': review, 'price': price}


if __name__ == '__main__':
    r = requests.get("https://shop.daltenda.com/prices-drop")
    soup = BeautifulSoup(r.content, features="html.parser")
    open('sconti.txt', 'w').close()
    
    for product in soup.find('ul', class_='product_list grid row').find_all('div', class_='product-container'):
        name = ''
        price = 0
        review = 0
        amz_price = 0

        # DALTENDA
        try:
            results = daltenda(product)
            name = results['name']
            price = results['price']
        except:
            if name == '':
                print('Daltenda error with a unnamed product')
            else:
                print('Daltenda error with '+name)
            continue
            
        # AMAZON
        try:
            query = 'Gioco da tavolo ' + name
            query = query.replace(' ', '+')
            results = amazon(query)
            review = results['review']
            amz_price = results['price']
        except:
            if name == '':
                print('Amazon error with a unnamed product')
            else:
                print('Amazon error with '+name)
            continue

        # CALCULATE DISCOUNT
        disc = amz_price - price
        if amz_price > 0:
            disc_perc = disc*100./amz_price
        else:
            disc_perc = 0

        # OUTPUT
        if review > 4.0:
            if amz_price > price:
                print(name + f'({review:.1f})')
                print(f'Daltenda: {price:.2f}€')
                print(f'Amazon: {amz_price:.2f}€')
                print(f'Sconto €: {disc:.2f}€')
                print(f'Sconto %: {disc_perc:.2f}%')
                print('\n')
        
                with open("sconti.txt", "a", encoding="utf-8") as file:
                    file.write(name + f'({review:.1f})\n')
                    file.write(f'Daltenda: {price:.2f}€\n')
                    file.write(f'Amazon: {amz_price:.2f}€\n')
                    file.write(f'Sconto €: {disc:.2f}€\n')
                    file.write(f'Sconto %: {disc_perc:.2f}%\n')
                    file.write('\n')

    driver.close()
